use std::{
    collections::HashMap,
    fs::File,
    io::{BufRead, BufReader},
    path::Path,
};

#[derive(Debug)]
pub enum Error {
    OpeningFile,
    MultipleKeys(String),
}

pub fn file_to_key_value_map(path: &Path) -> Result<HashMap<String, String>, Error> {
    let file = if let Ok(file) = File::open(path) {
        file
    } else {
        return Err(Error::OpeningFile);
    };
    let reader = BufReader::new(file);

    let mut map = HashMap::new();

    for line in reader.lines() {
        if let Ok(line) = line {
            if !line.trim().is_empty() {
                if let Some(index) = line.find(|c: char| c.is_whitespace()) {
                    let key = line[0..index].trim();
                    if map.contains_key(key) {
                        return Err(Error::MultipleKeys(key.to_string()));
                    }
                    let value = line[(index + 1)..].trim();
                    map.insert(key.to_string(), value.to_string());
                }
            }
        }
    }

    Ok(map)
}
