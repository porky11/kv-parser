Parses simple text format into a `HashMap`.

The keys and values are both strings and are separated by whitespace.
The value is allowed to contain whitespaces.

# Example

Key-Value-File:

```
key1 value1
key2 value2
key3 value3
```

The result would be a `HashMap` like this:

```
let mut map = HashMap::new();
map.insert("key1".to_string(), "value1".to_string());
map.insert("key2".to_string(), "value2".to_string());
map.insert("key3".to_string(), "value3".to_string());
assert_eq!(map, file_to_key_value_map(path));
```

